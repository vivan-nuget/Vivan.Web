﻿using Vivan.Web.Models;
using Vivan.Notifications.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Vivan.Web.Extensions
{
    public static class IReadOnlyCollectionExtensions
    {
        public static ICollection<ValidationErrorModel> ToValidationErrorList(this IReadOnlyCollection<Notification> notifications)
        {
            var errorList = notifications
                .Select(x => new ValidationErrorModel(x.Message, x.Key))
                .ToList();

            return errorList;
        }
    }
}
