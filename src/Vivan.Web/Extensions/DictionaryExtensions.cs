﻿using Vivan.Web.Models;
using System.Collections.Generic;
using System.Linq;

namespace Vivan.Web.Extensions
{
    public static class DictionaryExtensions
    {
        public static ICollection<ValidationErrorModel> ToValidationErrorList(this Dictionary<string, string[]> errors)
        {
            var errorList = errors.Keys
               .SelectMany(key => errors[key].Select(x => new ValidationErrorModel(x, key)))
               .ToList();

            return errorList;
        }
    }
}
