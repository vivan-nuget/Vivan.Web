﻿using Vivan.Web.Models;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Collections.Generic;
using System.Linq;

namespace Vivan.Web.Extensions
{
    public static class ModelStateDictionaryExtensions
    {
        public static ICollection<ValidationErrorModel> ToValidationErrorList(this ModelStateDictionary modelState)
        {
            var errorList = modelState.Keys
                .SelectMany(key => modelState[key].Errors.Select(x => new ValidationErrorModel(x.ErrorMessage, key)))
                .ToList();

            return errorList;
        }
    }
}
