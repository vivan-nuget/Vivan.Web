﻿using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;

namespace Vivan.Web.Extensions
{
    public static class HttpContentExtensions
    {
        public static async Task<T> ReadAsJsonAsync<T>(this HttpContent content, JsonSerializerSettings jsonSerializerSettings = null)
        {
            string json = await content.ReadAsStringAsync();

            if (jsonSerializerSettings == null)
            {
                return JsonConvert.DeserializeObject<T>(json);
            }
            else
            {
                return JsonConvert.DeserializeObject<T>(json, jsonSerializerSettings);
            }
        }
    }
}
