﻿using Vivan.Web.TokenServices;
using IdentityModel.Client;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;


namespace Vivan.Web.Extensions
{
    public static class HttpClientExtensions
    {
        public static async Task<HttpResponseMessage> PostAsJsonAsync<T>(this HttpClient client, string endPoint, T command)
        {
            var content = new StringContent(JsonConvert.SerializeObject(command),
                Encoding.UTF8,
                "application/json");

            return await client.PostAsync(endPoint, content);
        }

        public static void UseClientCredentials(this HttpClient httpClient,
            string endpoint,
            string clientId,
            string clientSecret,
            string scope = null)
        {
            try
            {
                var token = TokenService.ObterToken(endpoint, clientId, clientSecret, scope);

                httpClient.SetBearerToken(token);
            }
            catch (Exception ex)
            {
                throw new Exception(
                    $@"Ocorreu um erro ao tentar obter um token, por favor, verifique se todos os parâmetros informados estão corretos. Erro: {ex.Message} - {ex.StackTrace}");
            }
        }
    }
}
