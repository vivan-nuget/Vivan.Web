﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Vivan.Notifications.Entities;
using Vivan.Web.Models;

namespace Vivan.Web.Extensions
{
    public static class HttpResponseMessageExtension
    {
        public static async Task<IEnumerable<Notification>> ReadHervalBadRequestNotificationsAsync(
            this HttpResponseMessage response,
            JsonSerializerSettings jsonSerializerSettings = null)
        {
            var errorResult = await response.Content.ReadAsJsonAsync<ErrorResultModel>();

            //Pega as mensagens contidas no dicionários de erros quando for bad request (são notificações que foram serializadas)
            if ((int)response.StatusCode == StatusCodes.Status422UnprocessableEntity)
            {
                return errorResult.Errors.Select(m => new Notification(m.Field, m.Message));
            }

            //Para qualquer outro erro, ele pega a message
            return new List<Notification> { new Notification(errorResult.Message) };
        }
    }
}
