﻿using IdentityModel.Client;
using System;
using System.Net.Http;

namespace Vivan.Web.TokenServices
{
    internal static class TokenService
    {
        public static string ObterToken(string endpoint, string clientId, string clientSecret, string scope)
        {
            var client = new HttpClient();

            var disco = client.GetDiscoveryDocumentAsync(
                new DiscoveryDocumentRequest
                {
                    Address = endpoint,
                    Policy =
                    {
                        RequireHttps = false
                    }
                }).GetAwaiter().GetResult();

            var tokenResponse = client.RequestClientCredentialsTokenAsync(new ClientCredentialsTokenRequest
            {
                Address = disco.TokenEndpoint,
                ClientId = clientId,
                ClientSecret = clientSecret,
                Scope = scope
            }).GetAwaiter().GetResult();

            if (tokenResponse.IsError)
                throw new Exception($"{ tokenResponse.Error } - { tokenResponse.ErrorDescription }");

            return tokenResponse.AccessToken;
        }
    }
}
