﻿using Newtonsoft.Json;

namespace Vivan.Web.Models
{
    public class ValidationErrorModel
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Code { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Field { get; set; }

        public string Message { get; set; }

        protected ValidationErrorModel()
        { }

        public ValidationErrorModel(string message, string field, string code = null)
        {
            Code = string.IsNullOrEmpty(code) ? null : code;
            Field = string.IsNullOrEmpty(field) ? null : field;
            Message = message;
        }
    }
}
