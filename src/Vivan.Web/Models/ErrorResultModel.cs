﻿using Vivan.Web.Extensions;
using Vivan.Notifications.Entities;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Vivan.Web.Models
{
    public class ErrorResultModel
    {
        public string Message { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<ValidationErrorModel> Errors { get; set; }

        protected ErrorResultModel()
        { }
        
        public ErrorResultModel(string message)
        {
            Message = message;
        }

        public ErrorResultModel(string message, IReadOnlyCollection<Notification> notifications)
        {
            Message = message;
            Errors = notifications.ToValidationErrorList();
        }

        public ErrorResultModel(string message, ICollection<ValidationErrorModel> errors)
        {
            Message = message;
            Errors = errors;
        }

        public ErrorResultModel(string message, Dictionary<string, string[]> errors)
        {
            Message = message;
            Errors = errors.ToValidationErrorList();
        }

        public ErrorResultModel(string message, ModelStateDictionary modelState)
        {
            Message = message;
            Errors = modelState.ToValidationErrorList();
        }

        public ErrorResultModel(IReadOnlyCollection<Notification> notifications)
        {
            Errors = notifications.ToValidationErrorList();
        }
    }
}
