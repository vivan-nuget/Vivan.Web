﻿using Vivan.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Vivan.Web.Results
{
    public class NotFoundErrorResult : ObjectResult
    {
        private const string DefaultMessage = "O item não existe";

        public NotFoundErrorResult() :
            base(new ErrorResultModel(DefaultMessage))
        {
            StatusCode = StatusCodes.Status404NotFound;
        }

        public NotFoundErrorResult(string message) :
            base(new ErrorResultModel(message))
        {
            StatusCode = StatusCodes.Status404NotFound;
        }
    }
}
