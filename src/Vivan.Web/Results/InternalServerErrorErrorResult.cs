﻿using Vivan.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Vivan.Web.Results
{
    public class InternalServerErrorResult : ObjectResult
    {
        private const string DefaultMessage = "Houve um erro interno na API";

        public InternalServerErrorResult() :
            base(new ErrorResultModel(DefaultMessage))
        {
            StatusCode = StatusCodes.Status500InternalServerError;
        }

        public InternalServerErrorResult(string message) :
            base(new ErrorResultModel(message))
        {
            StatusCode = StatusCodes.Status500InternalServerError;
        }
    }
}
