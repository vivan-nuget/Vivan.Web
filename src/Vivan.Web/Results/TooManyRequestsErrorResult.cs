﻿using Vivan.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Vivan.Web.Results
{
    public class TooManyRequestsErrorResult : ObjectResult
    {
        private const string DefaultMessage = "A solicitação não pode ser atendida devido ao limite de taxa que foi esgotado para o recurso";

        public TooManyRequestsErrorResult() :
            base(new ErrorResultModel(DefaultMessage))
        {
            StatusCode = StatusCodes.Status429TooManyRequests;
        }

        public TooManyRequestsErrorResult(string message) :
            base(new ErrorResultModel(message))
        {
            StatusCode = StatusCodes.Status429TooManyRequests;
        }
    }
}
