﻿using Vivan.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Vivan.Web.Results
{
    public class ForbiddenErrorResult : ObjectResult
    {
        private const string DefaultMessage = "A solicitação é entendida, mas foi recusada ou o acesso não é permitido";

        public ForbiddenErrorResult() :
            base(new ErrorResultModel(DefaultMessage))
        {
            StatusCode = StatusCodes.Status403Forbidden;
        }

        public ForbiddenErrorResult(string message) :
            base(new ErrorResultModel(message))
        {
            StatusCode = StatusCodes.Status403Forbidden;
        }
    }
}
