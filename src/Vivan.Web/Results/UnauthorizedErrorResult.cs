﻿using Vivan.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Vivan.Web.Results
{
    public class UnauthorizedErrorResult : ObjectResult
    {
        private const string DefaultMessage = "As credenciais de autenticação estavam ausentes ou incorretas";

        public UnauthorizedErrorResult() :
            base(new ErrorResultModel(DefaultMessage))
        {
            StatusCode = StatusCodes.Status401Unauthorized;
        }

        public UnauthorizedErrorResult(string message) :
            base(new ErrorResultModel(message))
        {
            StatusCode = StatusCodes.Status401Unauthorized;
        }
    }
}
