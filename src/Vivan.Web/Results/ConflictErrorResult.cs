﻿using Vivan.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Vivan.Web.Results
{
    public class ConflictErrorResult : ObjectResult
    {
        public ConflictErrorResult(string message) :
            base(new ErrorResultModel(message))
        {
            StatusCode = StatusCodes.Status403Forbidden;
        }
    }
}
