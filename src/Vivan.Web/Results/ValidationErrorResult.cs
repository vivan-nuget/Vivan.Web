﻿using Vivan.Web.Models;
using Vivan.Notifications.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Collections.Generic;

namespace Vivan.Web.Results
{
    public class ValidationErrorResult : ObjectResult
    {
        private const string DefaultMessage = "Erros de validação em sua solicitação";


        public ValidationErrorResult(string message, IReadOnlyCollection<Notification> notifications) :
            base(new ErrorResultModel(message, notifications))
        {
            StatusCode = StatusCodes.Status422UnprocessableEntity;
        }

        public ValidationErrorResult(string message, ICollection<ValidationErrorModel> errors) :
            base(new ErrorResultModel(message, errors))
        {
            StatusCode = StatusCodes.Status422UnprocessableEntity;
        }

        public ValidationErrorResult(string message, Dictionary<string, string[]> errors) :
            base(new ErrorResultModel(message, errors))
        {
            StatusCode = StatusCodes.Status422UnprocessableEntity;
        }

        public ValidationErrorResult(string message, ModelStateDictionary modelState) :
            base(new ErrorResultModel(message, modelState))
        {
            StatusCode = StatusCodes.Status422UnprocessableEntity;
        }

        public ValidationErrorResult(IReadOnlyCollection<Notification> notifications)
           : base(new ErrorResultModel(notifications))
        {
            StatusCode = StatusCodes.Status422UnprocessableEntity;
        }

        public ValidationErrorResult(ICollection<ValidationErrorModel> errors) :
           base(new ErrorResultModel(DefaultMessage, errors))
        {
            StatusCode = StatusCodes.Status422UnprocessableEntity;
        }

        public ValidationErrorResult(Dictionary<string, string[]> errors) :
            base(new ErrorResultModel(DefaultMessage, errors))
        {
            StatusCode = StatusCodes.Status422UnprocessableEntity;
        }

        public ValidationErrorResult(ModelStateDictionary modelState) :
            base(new ErrorResultModel(DefaultMessage, modelState))
        {
            StatusCode = StatusCodes.Status422UnprocessableEntity;
        }
    }
}
