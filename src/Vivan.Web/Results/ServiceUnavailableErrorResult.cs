﻿using Vivan.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Vivan.Web.Results
{
    public class ServiceUnavailableErrorResult : ObjectResult
    {
        private const string DefaultMessage = "O servidor está ativo, mas sobrecarregado com solicitações. Tente mais tarde!";

        public ServiceUnavailableErrorResult() :
            base(new ErrorResultModel(DefaultMessage))
        {
            StatusCode = StatusCodes.Status503ServiceUnavailable;
        }

        public ServiceUnavailableErrorResult(string message) :
            base(new ErrorResultModel(message))
        {
            StatusCode = StatusCodes.Status503ServiceUnavailable;
        }
    }
}
