﻿using Vivan.Web.Models;
using Vivan.Notifications.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Collections.Generic;

namespace Vivan.Web.Results
{
    public class ErrorResult : ObjectResult
    {
        public ErrorResult(string message) :
           base(new ErrorResultModel(message))
        {
            StatusCode = StatusCodes.Status400BadRequest;
        }

        public ErrorResult(string message, int statusCode) :
           base(new ErrorResultModel(message))
        {
            StatusCode = statusCode;
        }

        public ErrorResult(IReadOnlyCollection<Notification> notifications)
           : base(new ErrorResultModel(notifications))
        {
            StatusCode = StatusCodes.Status400BadRequest;
        }

        public ErrorResult(string message, IReadOnlyCollection<Notification> notifications)
          : base(new ErrorResultModel(message, notifications))
        {
            StatusCode = StatusCodes.Status400BadRequest;
        }

        public ErrorResult(string message, IReadOnlyCollection<Notification> notifications, int statusCode)
          : base(new ErrorResultModel(message, notifications))
        {
            StatusCode = statusCode;
        }

        public ErrorResult(string message, ICollection<ValidationErrorModel> errors) :
            base(new ErrorResultModel(message, errors))
        {
            StatusCode = StatusCodes.Status400BadRequest;
        }

        public ErrorResult(string message, Dictionary<string, string[]> errors) :
            base(new ErrorResultModel(message, errors))
        {
            StatusCode = StatusCodes.Status400BadRequest;
        }

        public ErrorResult(string message, ModelStateDictionary modelState) :
            base(new ErrorResultModel(message, modelState))
        {
            StatusCode = StatusCodes.Status400BadRequest;
        }

        public ErrorResult(string message, ICollection<ValidationErrorModel> errors, int statusCode) :
           base(new ErrorResultModel(message, errors))
        {
            StatusCode = statusCode;
        }

        public ErrorResult(IReadOnlyCollection<Notification> notifications, int statusCode)
           : base(new ErrorResultModel(notifications))
        {
            StatusCode = statusCode;
        }

        public ErrorResult(string message, Dictionary<string, string[]> errors, int statusCode) :
            base(new ErrorResultModel(message, errors))
        {
            StatusCode = statusCode;
        }

        public ErrorResult(string message, ModelStateDictionary modelState, int statusCode) :
            base(new ErrorResultModel(message, modelState))
        {
            StatusCode = statusCode;
        }
    }
}
