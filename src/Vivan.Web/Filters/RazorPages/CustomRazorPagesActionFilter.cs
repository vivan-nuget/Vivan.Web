﻿using Vivan.Notifications.Interfaces;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Vivan.Web.Filters.RazorPages
{
    public class CustomRazorPagesActionFilter : IPageFilter
    {
        private readonly INotificationContext _notificationContext;

        public CustomRazorPagesActionFilter(INotificationContext notificationContext)
        {
            _notificationContext = notificationContext;
        }

        public void OnPageHandlerExecuted(PageHandlerExecutedContext context)
        {
            if (_notificationContext.HasNotifications)
            {
                foreach (var notification in _notificationContext.Notifications)
                {
                    context.ModelState.AddModelError(notification.Key, notification.Message);
                }

                context.Result = new PageResult();
            }
        }

        public void OnPageHandlerExecuting(PageHandlerExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                context.Result = new PageResult();
            }
        }

        public void OnPageHandlerSelected(PageHandlerSelectedContext context)
        {
        }
    }
}
