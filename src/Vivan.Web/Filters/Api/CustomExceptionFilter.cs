﻿using Vivan.Web.Results;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Threading.Tasks;

namespace Vivan.Web.Filters.Api
{
    public class CustomExceptionFilter : IAsyncExceptionFilter
    {
        public async Task OnExceptionAsync(ExceptionContext context)
        {
            context.Result = new ErrorResult(context.Exception.Message, StatusCodes.Status500InternalServerError);
        }
    }
}
