﻿using Vivan.Web.Results;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Vivan.Web.Filters.Api
{
    public class CustomActionFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                context.Result = new ValidationErrorResult(context.ModelState);
            }

            base.OnActionExecuting(context);
        }

        public override void OnResultExecuting(ResultExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                context.Result = new ValidationErrorResult(context.ModelState);
            }
            base.OnResultExecuting(context);
        }
    }
}
