﻿using Vivan.Web.Results;
using Vivan.Notifications.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Vivan.Web.Filters.Api
{
    public class CustomNotificationFilter : ActionFilterAttribute
    {
        private readonly INotificationContext _notificationContext;

        public CustomNotificationFilter(INotificationContext notificationContext)
        {
            _notificationContext = notificationContext;
        }

        public override void OnActionExecuted(ActionExecutedContext context)
        {
            if (_notificationContext.HasNotifications)
            {
                context.Result = new ErrorResult("Houve um erro ao processar sua requisição", _notificationContext.Notifications, StatusCodes.Status422UnprocessableEntity);
            }
            else
            {
                base.OnActionExecuted(context);
            }
        }
    }
}
