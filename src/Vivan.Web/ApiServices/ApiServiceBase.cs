﻿using System;

namespace Vivan.Web.ApiServices
{
    public abstract class ApiServiceBase : IDisposable
    {
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
